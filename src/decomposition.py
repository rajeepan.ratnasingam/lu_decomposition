import numpy as np
import scipy.linalg as sci_lin
import matplotlib.pyplot as plt
from timeit import Timer
from pprint import pprint
from math import sqrt

#################################################
#Checks on squareness of the Matrix

def is_square(x):
    """Checks the matrix if it's square"""
    square_check = True
    for j in range(len(x)):
        #checks each subarray it it has not the same length as the whole array
        if len(x) != len(x[j]):
            #if there is one subarray then it is clearly not a square matrix
            square_check = False
    return square_check

#################################################
#Checks on invertibleness of the Matrix

def is_invertible(x):
    """Checks the matrix if it's invertible"""
    return np.linalg.det(x)!=0

#################################################
#Checks on positive definitness of the Matrix

def is_pos_definit(x):
    """Checks if the matrix is positive defined"""
    return np.all(numpy.linalg.eigvals(x)>0)

#################################################
#Checks on symmetry of the Matrix

def is_symmetric(x):
    """Checks if the matrix is symmetric"""
    sym_check = True
    #checking every spot of the old matrix with the transposed matrix
    #if all are equal its symmetric
    for i in range(len(x)):
        for j in range(len(x[0])):
            if sym_check == True:
                sym_check = (np.array(x) == np.array(zip(*x)))[i][j]
    return sym_check

#################################################
#Do a simple matrix multiplication of square matrices

def matrix_multiplication(matrix_1, matrix_2):
    """Multiply square matrices of same dimension"""

    # Converts matrix_2 into a list of tuples                                                                                                                                                                                                      
    tuple_matrix_2 = zip(*matrix_2)

    # Convoluted list to calculate matrix multiplication                                                                                                                                                                                     
    return [[sum(element_m * element_n for element_m, element_n in zip(row_m, col_n)) for col_n in tuple_matrix_2] for row_m in matrix_1]

#################################################
# transposes the matrix

def trans_matrix(matrix):
    """Take the transpose of a matrix."""
    matrix_length = len(matrix)
    return [[ matrix[i][j] for i in range(matrix_length)] for j in range(matrix_length)]

#################################################
#generates the norm of the vector

def norm(vector_x):
    """Return the Euclidean norm of the vector x."""
    return sqrt(sum([x_i**2 for x_i in vector_x]))

#################################################
#generates a random symmetric matrix of dimension N

def create_random_symmetric_matrix(N):
     """creates a random symmetric matrix with dimension N
     in range (-2000,2000)"""
     b = np.random.random_integers(0,2000,size=(N,N))
     b_symm = (b + b.T)/2
     return b_symm

#################################################
#doing the QR-Decomposition with a random matrix and a timer

def lu_rand_myQR(N):
    def timed_func():
       return QR_decomposition(np.random.random(size=(N,N)),0)    
    return timed_func

#################################################
#doing the Built-in QR-Decomposition with a random matrix and a timer

def lu_rand_PythonsQR(N):
    def timed_func():
       return sci_lin.qr(np.random.random(size=(N,N)))    
    return timed_func

#################################################
#doing the LU-Decomposition with a random matrix and a timer

def lu_rand_myLU(N):
    def timed_func():
       return lu_decomposition(np.random.random(size=(N,N)),0)    
    return timed_func

#################################################
#doing the built-in LU-Decomposition with a random matrix and a timer

def lu_rand_PythonsLU(N):
    def timed_func():
       return sci_lin.lu(np.random.random(size=(N,N)))    
    return timed_func
    
#################################################
#doing the cholesky-decomposition with a random symmetric matrix and a timer

def lu_rand_mycholesky(N):
    def timed_func():
        return cholesky(create_random_symmetric_matrix(N),0)
    return timed_func

#################################################
#doing the built-in cholesky-decomposition with a random symmetric matrix and a timer

def lu_rand_PythonsCholesky(N):
    def timed_func():
        return sci_lin.cholesky(create_random_symmetric_matrix(N),0)
    return timed_func
                        

#################################################
#################################################
#Cholesky-decomposition

def cholesky(matrix_A, modus):
    """Performs a Cholesky decomposition of A, which must 
    be a symmetric and positive definite matrix. The function
    returns the lower variant triangular matrix, L."""
    # checks if cholesky is possible for that matrix
    if is_square(A) & is_pos_definit(A) & is_symmetric(A):
            dimension = len(matrix_A)

            # Create zero matrix for L
            lower_triangular_matrix_L = [[0.0] * dimension for i in xrange(dimension)]

            # Perform the Cholesky decomposition
            for i in xrange(dimension):
                for k in xrange(i+1):
                    # select a modus equal 1 to display all steps from the start to the end
                    if modus == 1:
                        print 'Matrix after next step is: '
                        print str(np.array(lower_triangular_matrix_L))
                
                    tmp_sum = sum(lower_triangular_matrix_L[i][j] * lower_triangular_matrix_L[k][j] for j in xrange(k))
            
                    if (i == k):
                        # generates the diagonal elements of the lower triangular Matrix step by step 
                        lower_triangular_matrix_L[i][k] = sqrt(matrix_A[i][i] - tmp_sum)
                    else:
                        # generates the rest of the lower triangula Matrix 
                        lower_triangular_matrix_L[i][k] = (1.0 / lower_triangular_matrix_L[k][k] * (matrix_A[i][k] - tmp_sum))

                # displays the final result of the cholesky decomposition
                print 'Final Lower triangular matrix is: '
                print str(np.array(lower_triangular_matrix_L))
                return lower_triangular_matrix_L
    else:
        #returns the error if cholesky is not possible for the input
        print "Cholesky doesn't work for that matrix"


#################################################
#Construction of the Q_t matrix
                
def Q_i(Q_min, i, j, k):
    """Construct the Q_t matrix by left-top padding the matrix Q                                                      
    with elements from the identity matrix."""
    if i < k or j < k:
        return float(i == j)
    else:
        return Q_min[i-k][j-k]

#################################################
#################################################
#QR-Decomposition
    
def QR_decomposition(A,modus):
    """Performs a Householder Reflections based QR Decomposition of the                                               
    matrix A. The function returns Q, an orthogonal matrix and R, an                                                  
    upper triangular matrix such that A = QR."""
    #checks if QR-Decomposition is possible for that matrix
    if is_square(A) & is_invertible(A):
            n = len(A)

            # Set R equal to A, and create Q as a zero matrix of the same size
            R = A
            Q = [[0.0] * n for i in xrange(n)]

            # The Householder procedure
            for k in range(n-1):  # We don't perform the procedure on a 1x1 matrix, so we reduce the index by 1
                # Create identity matrix of same size as A                                                                    
                I = [[float(i == j) for i in xrange(n)] for j in xrange(n)]

                # Create the vectors x, e and the scalar alpha
                # Python does not have a sgn function, so we use cmp instead
                x = [row[k] for row in R[k:]]
                e = [row[k] for row in I[k:]]
                alpha = -cmp(x[0],0) * norm(x)

                # Using anonymous functions, we create u and v
                u = map(lambda p,q: p + alpha * q, x, e)
                norm_u = norm(u)
                v = map(lambda p: p/norm_u, u)

                # Create the Q minor matrix
                Q_min = [ [float(i==j) - 2.0 * v[i] * v[j] for i in xrange(n-k)] for j in xrange(n-k) ]
                if modus == 1:
                    print 'Q minor matrix Nr.'+ str(k+1) + ' is: '
                    print str(np.array(Q_min))

                # "Pad out" the Q minor matrix with elements from the identity
                Q_t = [[ Q_i(Q_min,i,j,k) for i in xrange(n)] for j in xrange(n)]
                if modus == 1:
                    print 'Padded Q minor matrix Nr.'+ str(k+1) + ' is: '
                    print str(np.array(Q_t))

                # If this is the first run through, right multiply by A,
                # else right multiply by Q
                if k == 0:
                    Q = Q_t
                    R = matrix_multiplication(Q_t,A)
                else:
                    Q = matrix_multiplication(Q_t,Q)
                    R = matrix_multiplication(Q_t,R)

            # Since Q is defined as the product of transposes of Q_t,
            # we need to take the transpose upon returning it
            print 'Final transposed matrix Q is: '
            print str(np.array(trans_matrix(Q)))
            print 'Final R is: '
            print str(np.array(R))
            return trans_matrix(Q), R

    else:
        #if the matrix doesn't fit the properties for the QR-Decomposition it returns that error
        print "QR-Decomposition doesn't work for that Matrix"

#################################################
#Generates the pivot matrix for the LU-Decomposition
        
def pivot_matrix(matrix_1):
    """Returns the pivoting matrix for M, used in Doolittle's method."""
    dimension_permutation_matrix = len(matrix_1)

    # Create an identity matrix, with floating point values                                                                                                                                                                                            
    identity_matrix = [[float(i ==j) for i in xrange(dimension_permutation_matrix)] for j in xrange(dimension_permutation_matrix)]

    # Rearrange the identity matrix such that the largest element of                                                                                                                                                                                   
    # each column of M is placed on the diagonal of of M                                                                                                                                                                                               
    for j in xrange(dimension_permutation_matrix):
        row = max(xrange(j, dimension_permutation_matrix), key=lambda i: matrix_1[i][j])
        if j != row:
            # Swap the rows                                                                                                                                                                                                                            
            identity_matrix[j], identity_matrix[row] = identity_matrix[row], identity_matrix[j]

    return identity_matrix

#################################################
#LU-Decomposition

def lu_decomposition(A, modus):
    """Performs an LU Decomposition of A (which must be square)                                                                                                                                                                                        
    into PA = LU. The function returns P, L and U."""
    #checks if the LU-Decomposition is possible for that matrix
    if is_square(A) & is_invertible(A):
            dimension_A = len(A)

            # Create zero matrices for L and U                                                                                                                                                                                                                 
            lower_triangular_matrix = [[0.0] * dimension_A for i in xrange(dimension_A)]
            upper_triangular_matrix = [[0.0] * dimension_A for i in xrange(dimension_A)]

            # Create the pivot matrix P and the multipled matrix PA                                                                                                                                                                                            
            permutation_matrix = pivot_matrix(A)
            permuted_matrix = matrix_multiplication(permutation_matrix, A)

            # Perform the LU Decomposition
            for j in xrange(dimension_A):
                # All diagonal entries of L are set to unity                                                                                                                                                                                                   
                lower_triangular_matrix[j][j] = 1.0

                # generates the upper triangular matrix step by step                                                                                                                                                                                   
                for i in xrange(j+1):
                    s1 = sum(upper_triangular_matrix[k][j] * lower_triangular_matrix[i][k] for k in xrange(i))
                    upper_triangular_matrix[i][j] = permuted_matrix[i][j] - s1

                # generates the lower triangular matrix step by step                                                                                                                                                                 
                for i in xrange(j, dimension_A):
                    s2 = sum(upper_triangular_matrix[k][j] * lower_triangular_matrix[i][k] for k in xrange(j))
                    lower_triangular_matrix[i][j] = (permuted_matrix[i][j] - s2) / upper_triangular_matrix[j][j]
                # saves the generated matrices in their temp variable
                #modus to select, if you want to display the steps from the start to the end
                if modus == 1:
                    # prints the generated upper and lower matrices step bye step
                    print "upper triangular matrix after a step"
                    print str(np.array(upper_triangular_matrix))     
                    print "lower triangular matrix after a step"
                    print str(np.array(lower_triangular_matrix))
            # prints out the final result of the LU decomposition        
            print 'Final permutationmatrix is: '
            print str(np.array(permutation_matrix))
            print 'Final lowertriangularmatrix is: '
            print str(np.array(lower_triangular_matrix))
            print 'Final uppertriangularmatrix is: '
            print str(np.array(upper_triangular_matrix))
            return (permutation_matrix, lower_triangular_matrix, upper_triangular_matrix)

    else:
        #if the matrix doesn't fit the properties of a matrix for the LU-Decomposition it returns that error
        print "LU-Decomposition doesn't work for that matrix"

#################################################
#compares our QR-Decomposition with the built-in decomposition

def compare_QR(interval_max):
    """Compares our QR decomposition and the built-in decomposition by Python from dimension 1 to dimension interval_max in decimal steps. The graphs are plotted in two subfigures"""
    interval = range(1, interval_max, 10)
    times_myQR = [Timer(lu_rand_myQR(N)).timeit(1) for N in interval]                  # Calculation of execution times as a function of matrix dimension of our decomposition
    times_PythonsQR = [Timer(lu_rand_PythonsQR(N)).timeit(1) for N in interval]   # Calculation of execution times as a function of matrix dimension of the built-in decomposition
    plt.figure(1)
    plt.subplot(211)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of our QR algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_myQR, 'r')
    plt.subplot(212)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of Pythons built-in QR algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_PythonsQR, 'b')
    plt.show()


#################################################
#compares our LU-Decomposition with the built-in decomposition

def compare_LU(interval_max):
    """Compares our LU decomposition and the built-in decomposition by Python from dimension 1 to dimension interval_max in decimal steps.
        The graphs are plotted in two subfigures"""
    interval = range(1, interval_max, 10)
    times_myLU = [Timer(lu_rand_myLU(N)).timeit(1) for N in interval]                  # Calculation of execution times as a function of matrix dimension of our decomposition
    times_PythonsLU = [Timer(lu_rand_PythonsLU(N)).timeit(1) for N in interval]   # Calculation of execution times as a function of matrix dimension of the built-in decomposition
    plt.figure(1)
    plt.subplot(211)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of our LU algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_myLU, 'r')
    plt.subplot(212)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of Pythons built-in LU algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_PythonsLU, 'b')
    plt.show()    

#################################################
#compares our Cholesky-decomposition with the built-in decomposition

def compare_Cholesky(interval_max):
    """Compares our Cholesky decomposition and the built-in decomposition by Python from dimension 1 to dimension intverall_max in decimal steps.
        The graphs are plotted in two subfigures"""
    interval = range(1,interval_max,10)
    times_myCholesky = [Timer(lu_rand_mycholesky(N)).timeit(1) for N in interval]                  # Calculation of execution times as a function of matrix dimension of our decomposition
    times_PythonsCholesky = [Timer(lu_rand_PythonsCholesky(N)).timeit(1) for N in interval]   # Calculation of execution times as a function of matrix dimension of the built-in decomposition
    plt.figure(1)
    plt.subplot(211)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of our Cholesky algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_mycholesky, 'r')
    plt.subplot(212)   # 211 = numrows, numcols, fignum
    plt.xlabel('Matrix dimension')
    plt.ylabel('Execution Time')
    plt.title('Execution time of Pythons built-in Cholesky algorithm as a function of matrix dimension')
    plt.grid(True)
    plt.plot(interval, times_PythonsCholesky, 'b')
    plt.show()  
